# UI Kit Library

### Requirements

- Node.JS 12 (LTS). It ships with `yarn` out of the box

### How to import components from your project

- Components are being imported using a standard imports
  `import Button from "@mp-ui-kit/Buttons/Button"`
- Styles, variables and mixins are accessible using:
  `@import "~styles/main";`
  `@import "~styles/variables";`

### How to develop locally?
To run project locally you need to perform following steps:

    git clone https://<your_username>@bitbucket.org/advancedjs2020/ui-kit.git
    cd ui-kit
    yarn
    yarn storybook

### How to improve your performance?

- Select Node.js v12.13 
- Enable Node.js coding assistance
- Select yarn as default package manager

![](https://i.imgur.com/hJSE21N.png)

- There is an IDE fix to support alias resolution during the development. If you would not set the dummy webpack config, your IDE will not find imports like `@mp-ui-kit/Buttons/Button`

![](https://i.imgur.com/TyCANWZ.png)


- To improve the overall code-style, I strongly recommend to enable `eslint` and `prettier`. Using a shortcuts you will benefit from automatic code-style fixes done by them.

![](https://i.imgur.com/EWS3jhP.png)

- Mark src directory as Resource root

![](https://i.imgur.com/CJWVFmw.png)
