import Vue from 'vue';
import RangeSelector from '../src/components/RangeSelector/RangeSelector';

export default { title: 'RangeSelector' };

Vue.component('ui-range-selector', RangeSelector);

export const Default = () => '<ui-range-selector />';

export const Styled = () => ({
  components: { RangeSelector },
  template: `
    <div class="container">
      <ui-range-selector 
          :value="[30, 60]" 
          :interval="1" 
          :min="0" 
          :max="100"
          :height="8"
          :showIntervalText="true"
      />
    </div>
  `
});
