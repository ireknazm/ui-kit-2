import Vue from 'vue';

import InputComponent from "../src/components/InputComponent/InputComponent.vue";


export default {
  title: 'InputComponent'
}

export const Default = () => ({
  components: { InputComponent },
  template: `
  <div><InputComponent value="Default" /></div>
  `
});

export const BorderBottom = () => ({
  components: { InputComponent },
  template: `
  <div><InputComponent value="Border Bottom" borderBottom="true"  /></div>
  `
});
