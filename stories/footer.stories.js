import Vue from 'vue';

import Footer from '../src/components/Footer/Footer.vue';

export default {
  title: 'Footer'
}

export const Default = () => ({
  components: { Footer },
  template: `
  <div><Footer /></div>
  `
});