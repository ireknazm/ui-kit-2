import { addParameters, addDecorator } from '@storybook/vue';
import { withInfo } from 'storybook-addon-vue-info';
import customTheme from './customTheme';
import 'styles/main.scss';
import 'styles/story-styling.scss';

addDecorator(withInfo);

addParameters({
  options: {
    theme: customTheme,
    showPanel: false
  }
});
